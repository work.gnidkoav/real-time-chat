<?php

namespace App\Events;

use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;


class Message implements ShouldBroadcastNow
{
    use \Illuminate\Foundation\Events\Dispatchable;
    use \Illuminate\Broadcasting\InteractsWithSockets;
    use \Illuminate\Queue\SerializesModels;

    public $message;

    public function __construct($message)
    {
        $this->message = $message;
        $this->dontBroadcastToCurrentUser();
    }


    public function broadcastOn()
    {
        return new Channel('chat');
    }

}

