<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Room;


class User extends Authenticatable
{

    use \Illuminate\Notifications\Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }

}

