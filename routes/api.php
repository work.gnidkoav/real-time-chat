<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Illuminate\Http\Request $request) {
//    return $request->user();
//});

Route::post('/messages/public', function(Illuminate\Http\Request $request) {
    App\Events\Message::dispatch($request->all());
});

Route::post('/messages/private', function(Illuminate\Http\Request $request) {
    App\Events\PrivateMessage::dispatch($request->all());
});

