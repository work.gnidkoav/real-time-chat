@extends('layouts.app')

@section('content')
    @if (Auth::check())
        <private-chat :room="{{$room}}" :user="{{Auth::user()}}"></private-chat>
    @endif
@endsection
