<?php

use Illuminate\Database\Seeder;

use App\Room;


class RoomUserTableSeeder extends Seeder
{

    public function run()
    {
        /*/*/
        Room::find(1)->users()->attach([
            1,
            2,
            3,
            4,
            5,
        ]);
        Room::find(2)->users()->attach([
            1,
            2,
            3,
        ]);
        Room::find(3)->users()->attach([
            3,
            4,
            5,
        ]);
        /*/*/
        Room::find(4)->users()->attach([
            1,
            1,
        ]);
        Room::find(5)->users()->attach([
            1,
            2,
        ]);
        Room::find(6)->users()->attach([
            1,
            3,
        ]);
        Room::find(7)->users()->attach([
            1,
            4,
        ]);
        Room::find(8)->users()->attach([
            1,
            5,
        ]);
        Room::find(9)->users()->attach([
            2,
            2,
        ]);
        /*/*/
    }

}

