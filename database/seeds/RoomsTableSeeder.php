<?php

use Illuminate\Database\Seeder;

use App\Room;


class RoomsTableSeeder extends Seeder
{

    public function run()
    {
        /*/*/
        Room::create([
            'id'   => 1,
            'name' => 'First room',
        ]);
        Room::create([
            'id'   => 2,
            'name' => 'Second room',
        ]);
        Room::create([
            'id'   => 3,
            'name' => 'Third room',
        ]);
        /*/*/
        Room::create([
            'id'   => 4,
            'name' => null,
        ]);
        Room::create([
            'id'   => 5,
            'name' => null,
        ]);
        Room::create([
            'id'   => 6,
            'name' => null,
        ]);
        Room::create([
            'id'   => 7,
            'name' => null,
        ]);
        Room::create([
            'id'   => 8,
            'name' => null,
        ]);
        Room::create([
            'id'   => 9,
            'name' => null,
        ]);
        /*/*/
    }

}

