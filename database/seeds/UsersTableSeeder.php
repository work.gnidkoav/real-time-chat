<?php

use Illuminate\Database\Seeder;

use App\User;


class UsersTableSeeder extends Seeder
{

    public function run()
    {
        User::create([
            'id'       => 1,
            'name'     => 'Vasiliy',
            'email'    => 'vasiliy@email.tmp',
            'password' => bcrypt('22vasiliy33'),
        ]);
        User::create([
            'id'       => 2,
            'name'     => 'Vasilisa',
            'email'    => 'vasilisa@email.tmp',
            'password' => bcrypt('22vasilisa33'),
        ]);
        User::create([
            'id'       => 3,
            'name'     => 'Ivan',
            'email'    => 'ivan@email.tmp',
            'password' => bcrypt('22ivan33'),
        ]);
        User::create([
            'id'       => 4,
            'name'     => 'Olya',
            'email'    => 'olya@email.tmp',
            'password' => bcrypt('22olya33'),
        ]);
        User::create([
            'id'       => 5,
            'name'     => 'Kolya',
            'email'    => 'kolya@email.tmp',
            'password' => bcrypt('22kolya33'),
        ]);
    }

}

